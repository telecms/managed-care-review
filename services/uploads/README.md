# storybook

This service configures file uploads to AWS S3.

## Significant dependencies

-   serverless-s3-upload
-   See usage of `S3Client` in the [`app-web`](../app-web) service.
