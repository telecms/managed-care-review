export {
    newAWSEmailParameterStore,
    newLocalEmailParameterStore,
} from './emailParameterStore'

export type { EmailParameterStore } from './emailParameterStore'
